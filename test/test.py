import argparse
import json
import re
from random import randrange


def parse_cli_args():
    """Parses and virifies CLI arguments.

    Returns:
        Namespace: Parced arguments.
    """
    parser = argparse.ArgumentParser(description='Test script')
    common_parse_options = {"action": 'store', "required": True}

    test_target_allow_list = [
        "CPU_linux", "GPU_linux", "CPU_windows", "GPU_windows"
    ]

    def check_type(value):
        if value in test_target_allow_list:
            return value
        else:
            raise argparse.ArgumentTypeError(
                "Applicable values for target are {}".format(
                    ', '.join(test_target_allow_list)))

    parser.add_argument('-d',
                        dest='dataset_dir_path',
                        help='dataset_dir_path',
                        type=str,
                        **common_parse_options)

    parser.add_argument('-m',
                        dest='path_to_local_model_file',
                        help='path_to_local_model_file',
                        type=str,
                        **common_parse_options)

    parser.add_argument('-t',
                        dest='test_target',
                        help='"CPU" or "GPU"',
                        **common_parse_options,
                        type=check_type)

    return parser.parse_args()


def get_model_version(model_path):
    """Returns version of provided model.

    Example:
        mode-v1.2.bin -> 1.2
        mode.bin (no version in file name) -> 0.0.1 (default)

    Args:
        model_path (str): Path to model file.
    """
    match_obj = re.search(r"v(\d+\.){2,3}bin$", model_path)
    if match_obj:
        return match_obj.group()[1:-4]
    else:
        return '0.0.1'


def main():
    args = parse_cli_args()
    print(json.dumps({
        "version": get_model_version(args.path_to_local_model_file),
        "accuracy": randrange(30, 80),
        "test_time": randrange(101)
    }))


if __name__ == '__main__':
    main()
